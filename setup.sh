#!/bin/bash

date
sleep 5 # let filesystems settle
echo "UNSETUP VARIABLES"
if test -n "$ZSH_NAME"; then
  # sometimes we source from the shell directly
  TOPDIR=${0:a:h}
else
  TOPDIR=$(dirname "$(readlink -f "$BASH_SOURCE")")
fi
echo "TOPDIR is" $TOPDIR
#for var in $(cat $TOPDIR/asetup_envvars) ; do unset $var ; done
#export PATH=/afs/cern.ch/atlas/scripts:/afs/cern.ch/user/a/atlasdqm/scripts:/usr/sue/bin:/usr/lib64/qt-3.3/bin:/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin:/afs/cern.ch/user/a/atlasdqm/bin:/usr/local/sbin:/usr/sbin:/sbin:/opt/puppetlabs/bin:/afs/cern.ch/user/a/atlasdqm/bin/
#export AtlasSetup=/afs/cern.ch/atlas/software/dist/AtlasSetup
#alias asetup='source $AtlasSetup/scripts/asetup.sh'
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/
export ALRB_localConfigDir=/code/localConfig
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

asetup 22.0.103,Athena --input $TOPDIR/asetup-22 

if [ -d "$TOPDIR/build/" ] ; then 
#  source $TOPDIR/build/x86_64-centos7-gcc8-opt/setup.sh
  source $TOPDIR/build/x86_64-centos7-gcc11-opt/setup.sh
echo
fi

python -m pip install -r requirements.txt --user
# source asetup-22-devel-epilog

# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
# setupATLAS
lsetup pyami

#if [[ "$(uname -n)" == "aiatlas039.cern.ch" ]]
#then
#export CORAL_AUTH_PATH=/home/atlasdqm/private
#export CORAL_DBLOOKUP_PATH=/home/atlasdqm/private
#else
#export CORAL_AUTH_PATH=/afs/cern.ch/user/a/atlasdqm/private
#export CORAL_DBLOOKUP_PATH=/afs/cern.ch/user/a/atlasdqm/private
#fi

