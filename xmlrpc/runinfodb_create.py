from sqlalchemy import (create_engine, MetaData, Table, Column, Integer,
                        String, Float, Boolean, Numeric,
                        Index, Sequence, ForeignKey)
import xmlrpc.dbauth
from . import table_definitions

def create(conn=None, connstring=None):
    cleanup = False
    if conn == None or type(conn) == str:
        if connstring == None:
            raise ValueError('Must specify either a connection or a connection string')
        else:
            engine = create_engine(xmlrpc.dbauth.get_authentication(connstring))
            cleanup = True
    metadata = MetaData()
    table_definitions.runinfo.tometadata(metadata)
    metadata.create_all(engine)
