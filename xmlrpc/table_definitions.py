from sqlalchemy import (create_engine, MetaData, Table, Column, Integer,
                        String, Float, Boolean, Numeric,
                        Index, Time, DateTime, Sequence, ForeignKey,
                        UniqueConstraint)

_dummymetadata = MetaData()

runinfo = Table('runinfo', _dummymetadata,
                Column('run', Integer, primary_key=True),
                Column('run_type', String(20)),
                Column('project_tag', String(60)),
                Column('partition', String(120)),
                Column('ef_events', Integer),
                Column('run_start', Integer),
                Column('run_end', Integer),
                Column('lb', Integer),
                Column('data_source', String(10)),
                Column('det_mask', Numeric(precision=20, scale=0)),
                Column('rec_enable', Boolean),
                Column('sol_current', Float),
                Column('sol_set_current', Float),
                Column('tor_current', Float),
                Column('tor_set_current', Float),
                Column('physics_events', Integer),
                Column('max_energy', Float),
                Column('has_stable_beam', Boolean),
                Column('has_ready', Boolean),
                Column('lumi_total', Float),
                Column('lumi_ready', Float),
                schema='atlas_dq_results',
                )

streams = Table('streams', _dummymetadata,
                Column('streamkey', Integer,
                       Sequence('streams_seq', optional=True, schema='atlas_dq_results'),
                       primary_key=True),
                Column('stream', String(40), unique=True),
                schema='atlas_dq_results',
                )
sources = Table('sources', _dummymetadata,
                Column('sourcekey', Integer, 
                       Sequence('sources_seq', optional=True, schema='atlas_dq_results'),
                       primary_key=True),
                Column('source', String(15), unique=True),
                schema='atlas_dq_results',
                )
assessments = Table('assessments', _dummymetadata,
                    Column('assessmentkey', Integer, 
                           Sequence('assessments_seq', optional=True, schema='atlas_dq_results'),
                           primary_key=True),
                    Column('assessment', String(400), unique=True, 
                           index=True)
                    )
results = Table('results', _dummymetadata,
                Column('resultkey', Integer,  
                       Sequence('results_seq', optional=True, schema='atlas_dq_results'),
                       primary_key=True),
                Column('result', String(200), unique=True, index=True)
                )
periodtypes = Table('periodtypes', _dummymetadata,
                    Column('periodtypekey', Integer,  
                           Sequence('periodtypes_seq', optional=True, schema='atlas_dq_results'),
                           primary_key=True),
                    Column('periodtype', String(15), unique=True),
                    schema='atlas_dq_results',
                    )
statuses = Table('statuses', _dummymetadata,
                 Column('statuskey', Integer,  
                        Sequence('statuses_seq', optional=True, schema='atlas_dq_results'),
                        primary_key=True),
                 Column('status', String(15), unique=True)
                 )
amitags = Table('amitags', _dummymetadata,
                Column('amitagkey', Integer,  
                       Sequence('amitags_seq', optional=True, schema='atlas_dq_results'),
                       primary_key=True),
                Column('amitag', String(50), unique=True),
                schema='atlas_dq_results',
                )
heldfiles = Table('heldfiles', _dummymetadata,
                  Column('filename', String(100), primary_key=True),
                  schema='atlas_dq_results',
                  )

timestamps = Table('timestamps', _dummymetadata,
                   Column('source', Integer, ForeignKey(sources.c.sourcekey),
                          primary_key=True),
                   Column('run', Integer, primary_key=True),
                   Column('stream', Integer, ForeignKey(streams.c.streamkey),
                          primary_key=True),
                   Column('procno', Integer, primary_key=True),
                   Column('periodtype', Integer, 
                          ForeignKey(periodtypes.c.periodtypekey),
                          primary_key=True),
                   Column('period', Integer, primary_key=True),
                   Column('timestamp', DateTime),
                   Column('amitag', Integer, ForeignKey(amitags.c.amitagkey)),
                   schema='atlas_dq_results',
                   )

valcache = Table('valcache', _dummymetadata,
                 Column('source', Integer, ForeignKey(sources.c.sourcekey)),
                 Column('run', Integer),
                 Column('stream', Integer, ForeignKey(streams.c.streamkey)),
                 Column('procno', Integer),
                 Column('periodtype', Integer, ForeignKey(periodtypes.c.periodtypekey)),
                 Column('period', Integer),
                 Column('assessment', Integer, ForeignKey(assessments.c.assessmentkey)),
                 Column('result', Integer, ForeignKey(results.c.resultkey)),
                 Column('resultval', Float(precision=None))
                 )

statcache = Table('statcache', _dummymetadata,
                  Column('source', Integer, ForeignKey(sources.c.sourcekey)),
                  Column('run', Integer),
                  Column('stream', Integer, ForeignKey(streams.c.streamkey)),
                  Column('procno', Integer),
                  Column('periodtype', Integer, ForeignKey(periodtypes.c.periodtypekey)),
                  Column('period', Integer),
                  Column('assessment', Integer, ForeignKey(assessments.c.assessmentkey)),
                  Column('status', Integer, ForeignKey(statuses.c.statuskey))
                  )
