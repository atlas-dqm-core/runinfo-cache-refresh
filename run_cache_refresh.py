from typing import Optional
from DataQualityUtils.panic import panic


def run_cache_refresh(livefile: Optional[str]):
    import time
    from pathlib import Path
    import xmlrpc.runinfodb_update
    import traceback
    countdown = 4
    e = 'Unknown'
    while countdown > 0:
        try:
            xmlrpc.runinfodb_update.update()
            if livefile:
                Path(livefile).touch()
        except Exception as e:
            print('Unable to update run DB, reason:', e)
            print((traceback.format_exc()))
            # die if this keeps happening
            print('Number of retries allowed:', countdown)
            countdown -= 1
        time.sleep(120)
    panic('There seem to be failures occurring in the run cache updater.\n'
          'Please go to paas.cern.ch and take a look at the logfiles.\n'
          f'Latest exception: {e}\n'
          f'{traceback.format_exc()}')

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--livefile', help='File which will be touched on every pass')
    args = parser.parse_args()
    if args.livefile:
        print('Running with livefile:', args.livefile)
    run_cache_refresh(args.livefile)
